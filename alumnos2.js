var listacurso = [];
var listaalumnos = [];

var listaalumnos2 = [];

function cargarcurso(){
    this.listacurso.push(
        {id: 1, nombre: "Programacion II", semestre: "1/2024"}
    );
    return null;
}

function cargarDatosalumnos(){
    this.listaalumnos.push(
        {nombre: "Ale", sexo: "F", edad: 25, nmatricula: 5635, idcurso: 1},
        {nombre: "Tarek", sexo: "M", edad: 30, nmatricula: 5635, idcurso: 1},
        {nombre: "Gabriel", sexo: "M", edad: 50, nmatricula: 5635, idcurso: 1},
        {nombre: "Yandhira", sexo: "F", edad: 19, nmatricula: 5635, idcurso: 1},
        {nombre: "Tani", sexo: "F", edad: 19, nmatricula: 5635, idcurso: 1},
        {nombre: "Natalia", sexo: "F", edad: 25, nmatricula: 5635, idcurso: 1},
        {nombre: "Taylor", sexo: "F", edad: 25, nmatricula: 5635, idcurso: 1}
    )
}

function cantidadfemeninomasculino(sexo, listadoalumnos){
    var result = 0;
    listadoalumnos.forEach(element => {
        if(element.sexo == sexo){
            result++;
        }
    });
    return result;
}

function grupoetarioPorcentual(inicio, final, listadoalumnos){
    var cantidad = 0;
    var cantidadtotal = 0;
    listadoalumnos.forEach(element => {
        cantidadtotal++;
        if( element.edad >= inicio && element.edad <= final){
            cantidad++;
        }
    });
    return (cantidad / cantidadtotal) * 100;
}

function ordernaralumno(tipoordenar, listadoalumnos){
    listadoalumnos.sort((a,b)=> a.nombre.localeCompare(b.nombre));
    if(tipoordenar === "desc"){
        listadoalumnos.sort((a,b)=> b.nombre.localeCompare(a.nombre)); 
    }
    return listadoalumnos;
}

function consultacursoestudiantes(listacurso, listadoalumnos){
    try {
        listadoalumnos.forEach(element => {
            var curso = null;
            listacurso.forEach(items => {
                if(items.id === element.idcurso){
                    curso = items;
                }
            });
            // Asignar el curso al estudiante
            element.curso= curso;
        });
    }
    catch (error){
        console.error(error);
    }
    return listadoalumnos;
}

function imprimircantidadporsexo(){
    var mujeres = cantidadfemeninomasculino("F", listaalumnos);
    var varones = cantidadfemeninomasculino("M", listaalumnos);
    console.log("1. Cantidad de mujeres y varones");
    console.log("Mujeres: "+ mujeres );
    console.log("Varones: "+ varones );
}

function imprimirgrupoetario(){
    var valor = grupoetarioPorcentual(25, 30, listaalumnos);
    console.log("2. Grupo etario porcentual");
    console.log("Valor: "+ valor + "%");
    console.log("Valor fuera del rango "+ (100 - valor) + "%");
}

function imprimirlistadoordenado(){
    var selectalmunoscurso = consultacursoestudiantes(listacurso, listaalumnos);
    selectalmunoscurso = ordernaralumno("asc", selectalmunoscurso);
    console.log("3. Listado de alumnos por curso");
    selectalmunoscurso.forEach(element =>{
        if (element.curso) {
            console.log(element.nombre + " ("+ element.curso.nombre + " "+ element.curso.semestre+ " )");
        }
    });
}

cargarcurso();
cargarDatosalumnos();
imprimircantidadporsexo();
imprimirgrupoetario();
imprimirlistadoordenado();

// Obtener referencia al botón de guardar
var btnGuardar = document.getElementById('btnGuardar');

// Agregar event listener para el evento click en el botón de guardar
btnGuardar.addEventListener('click', function(event) {
    event.preventDefault(); // Prevenir el comportamiento por defecto del formulario

    // Obtener valores del formulario
    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var genero = document.getElementById('genero').value;
    var edad = document.getElementById('edad').value;
    var matricula = document.getElementById('matricula').value;
    var idCurso = document.getElementById('idCurso').value;

    // Crear objeto de alumno
    var alumno = {
        nombre: nombre,
        apellidos: apellidos,
        sexo: genero,
        edad: edad,
        nmatricula: matricula,
        idcurso: idCurso
    };

    // Agregar el alumno a la lista de alumnos
    listaalumnos2.push(alumno);

    // Llamar a la función para actualizar la tabla
    actualizarTabla();
});

// Función para actualizar la tabla
// Función para actualizar la tabla
// Función para actualizar la tabla
// Función para actualizar la tabla
function actualizarTabla() {
    var tabla = document.getElementById('tabla-alumnos');
    // Limpiar la tabla
    tabla.innerHTML = '';

    // Crear la fila de encabezado
    var encabezado = document.createElement('tr');
    encabezado.innerHTML = `
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Edad</th>
        <th>Curso ID</th>
    `;
    tabla.appendChild(encabezado);

    // Crear una fila para cada alumno y agregarla a la tabla
    listaalumnos2.forEach(function(alumno) {
        var fila = document.createElement('tr');
        fila.innerHTML = `
            <td>${alumno.nmatricula}</td>
            <td>${alumno.nombre} ${alumno.apellidos}</td>
            <td>${alumno.edad}</td>
            <td>${alumno.idcurso}</td>
        `;
        tabla.appendChild(fila);
    });
}

// Obtener referencia al botón de limpiar
var btnLimpiar = document.getElementById('btnlimpiar');

// Agregar event listener para el evento click en el botón de limpiar
btnLimpiar.addEventListener('click', function(event) {
    event.preventDefault(); // Prevenir el comportamiento por defecto del botón

    // Limpiar la tabla manteniendo la fila de encabezado
    var tabla = document.getElementById('tabla-alumnos');
    // Eliminar todas las filas de la tabla, excepto la primera (la fila de encabezado)
    while (tabla.rows.length > 1) {
        tabla.deleteRow(1);
    }
});
// Obtener referencia al botón de limpiar
var btnCancelar = document.querySelector('btnCancelar');

// Agregar event listener para el evento click en el botón de limpiar
btnLimpiar.addEventListener('click', function(event) {
    event.preventDefault(); // Prevenir el comportamiento por defecto del formulario

    // Limpiar los valores de los campos del formulario
    document.getElementById('nombre').value = '';
    document.getElementById('apellidos').value = '';
    document.getElementById('genero').value = 'F'; // Reiniciar género a Femenino
    document.getElementById('edad').value = '';
    document.getElementById('matricula').value = '';
    document.getElementById('idCurso').value = '';
});



